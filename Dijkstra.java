import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map.Entry;
import java.util.List;
import java.util.Collections;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;

public class Dijkstra {

    public static final Map<Integer, String> conversionMap = Map.of(
        0, "SF",
        1, "Seattle",
        2, "Idaho",
        3, "Chicago",
        4, "NYC"
    );

    public static void main(String[] args) {
        int[][] distances = {
            {0, 3, 5, 0, 0},
            {3, 0, 1, 2, 0},
            {5, 1, 0, 3, 6},
            {0, 2, 3, 0, 4},
            {0, 0, 6, 4, 0},
        };

        Dijkstra dijkstra = new Dijkstra();
        List<TotalTravelData> shortestPath = dijkstra.calculateShortestPath(0, distances);

        System.out.println("Shortest path to " + conversionMap.get(shortestPath.get(shortestPath.size() - 1).node) + ": " + shortestPath.get(shortestPath.size() - 1).totalDistance);

        System.out.println("Path traveled: ");

        for (TotalTravelData travelData: shortestPath) {
            System.out.print("[" + conversionMap.get(travelData.node) + " " + travelData.totalDistance + "]" + " -> ");
        }
        System.out.println(" Done!");
    }

    public List<TotalTravelData> calculateShortestPath(int start, int[][] graph) {
        Set<Integer> visitedNodes = new HashSet<>();
        PriorityQueue<TotalTravelData> minPQ = new PriorityQueue<TotalTravelData>(graph.length, new TotalTravelDataComparator());
        TreeMap<Integer, TotalTravelData> travelMap = new TreeMap<>();

        minPQ.add(new TotalTravelData(start, 0));
        travelMap.put(0, new TotalTravelData(0, 0, 0));
        visitedNodes.add(0);

        while (!minPQ.isEmpty()) {
            // Pop off the lowest from the minPQ, and add it to the list of traveled nodes
            TotalTravelData currentNode = minPQ.remove();
            visitedNodes.add(currentNode.node);

            for (int checkNode = 0; checkNode < graph[currentNode.node].length; ++checkNode) {
                int nodeDistance = graph[currentNode.node][checkNode];

                // There's no path from this node (it's itself), or we've already visited
                if (nodeDistance == 0 || visitedNodes.contains(checkNode)) {
                    continue;
                }

                // Does the minPQ already contain a version of the node?
                TotalTravelData queueNode = nodeInQueue(checkNode, minPQ);

                int totalDistanceToNode = currentNode.totalDistance + nodeDistance;
                // Nope, let's insert it
                if (queueNode == null) {
                    minPQ.add(new TotalTravelData(checkNode, totalDistanceToNode));
                } else if (totalDistanceToNode < queueNode.totalDistance) {
                    minPQ.remove(queueNode);
                    minPQ.add(new TotalTravelData(checkNode, totalDistanceToNode));
                } else { continue; }

                // update the maps and shit
                travelMap.put(checkNode, new TotalTravelData(checkNode, totalDistanceToNode, currentNode.node));
            }
        }

        return makeDistancePath(travelMap);
    }

    private TotalTravelData nodeInQueue(int node, PriorityQueue<TotalTravelData> minPQ) {
        for (TotalTravelData travelNode: minPQ) {
            if (node == travelNode.node) {
                return travelNode;
            }
        }
        return null;
    }

    private List<TotalTravelData> makeDistancePath(TreeMap<Integer, TotalTravelData> distanceMap) {

        TotalTravelData currentNode = distanceMap.lastEntry().getValue();
        TotalTravelData previousNode = distanceMap.get(currentNode.previousNode);

        for (Entry<Integer, TotalTravelData> datas : distanceMap.entrySet()) {
            System.out.println("Current node: " + conversionMap.get(datas.getKey()) + ", distance to node: " + datas.getValue().totalDistance + ", previousNode: " + conversionMap.get(datas.getValue().previousNode));
        }

        List<TotalTravelData> travelList = new ArrayList<>();

        while (currentNode != previousNode) {
            travelList.add(currentNode);
            currentNode = previousNode;
            previousNode = distanceMap.get(previousNode.previousNode);
        }
        travelList.add(currentNode);

        Collections.reverse(travelList);
        return travelList;
    }
}

class TotalTravelData {
    public int node;
    public int totalDistance;
    public int previousNode;

    public TotalTravelData(int node, int totalDistance) {
        this.node = node;
        this.totalDistance = totalDistance;
    }

    public TotalTravelData(int node, int totalDistance, int previousNode) {
        this(node, totalDistance);
        this.previousNode = previousNode;
    }

    public TotalTravelData(TotalTravelData travelData) {
        this(travelData.node, travelData.totalDistance, travelData.previousNode);
    }
}

class TotalTravelDataComparator implements Comparator<TotalTravelData> {
    @Override
    public int compare(TotalTravelData x, TotalTravelData y) {
        if (x.totalDistance < y.totalDistance) {
            return -1;
        }
        if (y.totalDistance < x.totalDistance) {
            return 1;
        }
        return 0;
    }
}
